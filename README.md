# Worldreader
App done with:
* Kotlin
  * Used because of his advantages in null-safe operators, lists management, lines of code... etc
* Clean architecture + MVP
  * Used because it improves the code maintainability in a well know architecture
* Dagger2
  * Used to apply dependency injection, making the layers more independent and making unit testing easier. It is a very stable library too, in contrast to other newer Kotlin-friendly libraries like Koin
* Retrofit2
  * Used because- in my opinion - it is the easiest library for accessing to rest services in Android
* RxJava2, RxAndroid, RxKotlin
  * Despite making the architecture dependent on his use, I've decided to use it because makes easy the thread management to avoid lifecycle problems, and because it allows to use operators with different threads making easy to work with nested calls
* Room
  * Used to cache network objects on the device storagement, and because - in my opinion - is more simple than other ORM implementations. Also it works under SQLite, then it uses the native Android database system.
* Unit tests with Mockito and MockitoKotlin
  * Used to mock objects in the unit tests since it is a stable library and ensures the good practices - agains other libraries like PowerMock that can be used in a wrong way easily, or like mockK that are still very young - . About MockitoKotlin, I've used it because improves the integration of the original library with the Kotlin language.
* Timber
  * Used as a quick enhacement in the Android logging
* Guava
  * Used as a quick way to calculate hash SHA256
* DexOpener
  * Used as a workaround to allow mock final classes in instrumented tests
* Glide
  * Used to load images with self-memory management. It is very similar to Picasso, with not clear advantages or disadvantages in this case.

*Julian Zaragoza 2018*