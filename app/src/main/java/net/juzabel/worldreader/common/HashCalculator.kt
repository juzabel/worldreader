package net.juzabel.worldreader.common

import com.google.common.hash.Hashing
import net.juzabel.worldreader.BuildConfig
import java.nio.charset.StandardCharsets
import javax.inject.Inject

class HashCalculator @Inject constructor() {

    fun calculate(url: String, timestamp: Long, token: String): String {
        val replacedUrl = url.replace(BuildConfig.BASE_URL, "")

        return  Hashing.sha256()
            .hashString(replacedUrl + timestamp + token, StandardCharsets.UTF_8)
            .toString()
    }
}