package net.juzabel.worldreader.common

import android.app.Application
import android.content.Context
import net.juzabel.worldreader.di.components.AppComponent
import net.juzabel.worldreader.di.components.DaggerAppComponent
import net.juzabel.worldreader.di.modules.AppModule

class WorldreaderApplicationImpl : Application(), WorldreaderApplication {
    private lateinit var appComponent: AppComponent
    private lateinit var appModule: AppModule

    override fun onCreate() {
        super.onCreate()
        injectApplicationComponent()
    }

    override fun injectApplicationComponent() {

        appModule = AppModule(this);
        appComponent = DaggerAppComponent.builder().appModule(appModule).build()
        appComponent.inject(this)
    }

    override fun getApplicationModule(): AppModule? = appModule

    override fun getContext(): Context = this

    override fun getApplicationComponent(): AppComponent = appComponent
}
