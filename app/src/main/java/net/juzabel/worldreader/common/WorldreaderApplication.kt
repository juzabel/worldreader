package net.juzabel.worldreader.common

import android.content.Context
import net.juzabel.worldreader.di.components.AppComponent
import net.juzabel.worldreader.di.modules.AppModule

interface WorldreaderApplication {

    fun injectApplicationComponent()

    fun getApplicationModule(): AppModule?

    fun getApplicationComponent(): AppComponent

    fun getContext(): Context

}