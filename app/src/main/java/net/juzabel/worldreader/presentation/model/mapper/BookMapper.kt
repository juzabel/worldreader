package net.juzabel.worldreader.presentation.model.mapper

import net.juzabel.worldreader.domain.entity.BookEntity
import net.juzabel.worldreader.presentation.model.entity.Book
import javax.inject.Inject

class BookMapper @Inject constructor() {
    fun map(bookEntity: BookEntity) : Book = Book(bookEntity.author, bookEntity.title, bookEntity.cover)
}