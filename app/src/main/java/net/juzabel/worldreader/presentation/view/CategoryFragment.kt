package net.juzabel.worldreader.presentation.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_category.*
import net.juzabel.worldreader.di.modules.FragmentModule
import net.juzabel.worldreader.presentation.base.FragmentExtensions.getActivityComponent
import net.juzabel.worldreader.presentation.contract.CategoryContract
import net.juzabel.worldreader.presentation.model.entity.Category
import net.juzabel.worldreader.presentation.presenter.CategoryPresenter
import net.juzabel.worldreader.presentation.view.adapter.CategoriesAdapter
import javax.inject.Inject
import net.juzabel.worldreader.R

class CategoryFragment : Fragment(), CategoryContract.View {

    companion object {
        fun newInstance() = CategoryFragment()
    }

    @Inject
    lateinit var categoryPresenter: CategoryPresenter

    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity?.setTitle(R.string.select_category)

        getActivityComponent().plusFragmentComponent(FragmentModule()).inject(this)
        categoryPresenter.view = this
        categoryPresenter.create()
        categoryPresenter.getCategories()

        viewManager = LinearLayoutManager(context)

        rvCategories.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_category, container, false)
    }

    override fun onCategoriesResult(categories: List<Category>?) {
        rvCategories.adapter = categories?.let { CategoriesAdapter(it, categoryPresenter) }
    }

}