package net.juzabel.worldreader.presentation.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_book.view.*
import net.juzabel.worldreader.presentation.model.entity.Book
import net.juzabel.worldreader.presentation.util.ImageUtil
import net.juzabel.worldreader.R

class BooksAdapter(private val bookList: List<Book>, private val imageUtil: ImageUtil) :
    RecyclerView.Adapter<BooksAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_book, parent, false)
        return ViewHolder(view, imageUtil)
    }

    override fun getItemCount(): Int = bookList.size

    override fun onBindViewHolder(viewHolder: ViewHolder, pos: Int) {
        viewHolder.bindTitle(bookList[pos].title)
        viewHolder.bindAuthor(bookList[pos].author)
        viewHolder.bindImage(bookList[pos].cover)
    }

    class ViewHolder(view: View, private val imageUtil: ImageUtil) : RecyclerView.ViewHolder(view) {

        fun bindTitle(title: String?) {
            itemView.tvTitleValue.text = title
        }

        fun bindAuthor(author: String?) {
            itemView.tvAuthorValue.text = author
        }

        fun bindImage(imageUrl: String?) {
            if (imageUrl != null) {
                imageUtil.load(imageUrl, itemView.imgBook)
            } else {
                imageUtil.load(R.drawable.book, itemView.imgBook)
            }
        }

    }
}