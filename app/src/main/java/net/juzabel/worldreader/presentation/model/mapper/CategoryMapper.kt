package net.juzabel.worldreader.presentation.model.mapper

import net.juzabel.worldreader.domain.entity.CategoryEntity
import net.juzabel.worldreader.presentation.model.entity.Category
import javax.inject.Inject

class CategoryMapper @Inject constructor() {
    fun map(categoryEntity: CategoryEntity) : Category = Category(categoryEntity.id, categoryEntity.title, categoryEntity.icon)
}