package net.juzabel.worldreader.presentation.util

import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.bumptech.glide.request.RequestOptions
import net.juzabel.worldreader.BuildConfig
import net.juzabel.worldreader.common.HashCalculator
import java.util.*
import javax.inject.Inject
import javax.inject.Named
import net.juzabel.worldreader.R


class ImageUtil @Inject constructor(private val activity: AppCompatActivity,
                                    @Named("base_image") private val url: String,
                                    private val hashCalculator: HashCalculator) {

    fun load(path: String, imageView: ImageView) {

        val timestamp = Date().time
        val fullPath: String = url + path
        val hash = hashCalculator.calculate(fullPath, timestamp, BuildConfig.TOKEN)

        val headers = LazyHeaders.Builder()
            .addHeader(BuildConfig.HEADER_CLIENT, BuildConfig.HEADER_CLIENT_VALUE)
            .addHeader(BuildConfig.HEADER_HASH, hash)
            .addHeader(BuildConfig.HEADER_TIMESTAMP, timestamp.toString())

        val glideUrl = GlideUrl(fullPath, headers.build())

        val options = RequestOptions()
            .fitCenter()
            .centerCrop()
            .error(R.drawable.image_error)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)

        Glide.with(activity).load(glideUrl).apply(options).into(imageView)
    }

    fun load(id: Int, imageView: ImageView) {
        Glide.with(activity).load(id).into(imageView)
    }
}