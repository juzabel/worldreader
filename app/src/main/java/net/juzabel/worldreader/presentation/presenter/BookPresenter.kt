package net.juzabel.worldreader.presentation.presenter

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import net.juzabel.worldreader.domain.base.UseCase
import net.juzabel.worldreader.domain.entity.BookEntity
import net.juzabel.worldreader.presentation.Navigator
import net.juzabel.worldreader.presentation.base.PresenterDisposableManager
import net.juzabel.worldreader.presentation.contract.BookContract
import net.juzabel.worldreader.presentation.model.mapper.BookMapper
import timber.log.Timber
import javax.inject.Inject

class BookPresenter @Inject constructor(
    private val useCase: UseCase<Long, Single<List<BookEntity>>>,
    private val bookMapper: BookMapper,
    private val navigator: Navigator
) : PresenterDisposableManager<BookContract.View>(), BookContract.Presenter {

    override fun getBooks(category: Long?) {

        useCase.execute(category)
            .flattenAsFlowable { it -> it }
            .map { bookMapper.map(it) }
            .toList()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = {
                    view?.onBooksResult(it)
                },
                onError = {
                    navigator.showMessage(it.localizedMessage)
                    view?.onBooksResult(null)
                    Timber.e(it)
                })
            .addTo(compositeDisposable)

    }

}