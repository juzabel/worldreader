package net.juzabel.worldreader.presentation.view

import android.opengl.Visibility
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_books.*
import net.juzabel.worldreader.di.modules.FragmentModule
import net.juzabel.worldreader.presentation.base.FragmentExtensions.getActivityComponent
import net.juzabel.worldreader.presentation.contract.BookContract
import net.juzabel.worldreader.presentation.model.entity.Book
import net.juzabel.worldreader.presentation.presenter.BookPresenter
import net.juzabel.worldreader.presentation.util.ImageUtil
import net.juzabel.worldreader.presentation.view.adapter.BooksAdapter
import javax.inject.Inject
import net.juzabel.worldreader.R


class BookFragment : Fragment(), BookContract.View {
    companion object {

        const val CATEGORY = "CATEGORY"

        fun newInstance(id: Long): BookFragment {
            val bundle =  Bundle()
            bundle.putLong(CATEGORY, id)

            val bookFragment = BookFragment()
            bookFragment.arguments = bundle

            return bookFragment
        }
    }

    @Inject
    lateinit var bookPresenter: BookPresenter

    @Inject
    lateinit var imageUtil: ImageUtil

    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity?.setTitle(R.string.books)

        getActivityComponent().plusFragmentComponent(FragmentModule()).inject(this)
        bookPresenter.view = this
        bookPresenter.create()
        bookPresenter.getBooks(arguments?.getLong(CATEGORY))

        viewManager = LinearLayoutManager(context)

        rvBooks.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_books, container, false)
    }

    override fun onBooksResult(categories: List<Book>?) {
        rvBooks.adapter = categories?.let { BooksAdapter(it, imageUtil) }
        if (categories == null || categories.isEmpty()) {
            tvEmpty.visibility = View.VISIBLE
            rvBooks.visibility = View.GONE
        } else {
            tvEmpty.visibility = View.GONE
            rvBooks.visibility = View.VISIBLE
        }
    }
}
