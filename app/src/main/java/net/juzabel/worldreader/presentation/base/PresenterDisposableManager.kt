package net.juzabel.worldreader.presentation.base

import io.reactivex.disposables.CompositeDisposable

open class PresenterDisposableManager<V> {
    var view: V? = null

    protected val compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun create() {
        if(view == null){
            throw IllegalStateException("Please provide first the view")
        }
    }

    fun destroy() {
        compositeDisposable.clear()
    }
}