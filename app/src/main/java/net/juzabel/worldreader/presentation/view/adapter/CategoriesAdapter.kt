package net.juzabel.worldreader.presentation.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_category.view.*
import net.juzabel.worldreader.presentation.model.entity.Category
import net.juzabel.worldreader.presentation.presenter.CategoryPresenter
import net.juzabel.worldreader.R

class CategoriesAdapter(private val categoryList: List<Category>, private val categoryPresenter: CategoryPresenter)
    : RecyclerView.Adapter<CategoriesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false)
        return ViewHolder(view, categoryPresenter)
    }

    override fun getItemCount(): Int = categoryList.size

    override fun onBindViewHolder(viewHolder: ViewHolder, pos: Int) {
        viewHolder.bindName(categoryList[pos].title)
        viewHolder.bindClick(categoryList[pos].id)
    }

    class ViewHolder(view: View, private val categoryPresenter: CategoryPresenter) : RecyclerView.ViewHolder(view) {

        fun bindName(name: String?) {
            itemView.tvCategory.text = name
        }

        fun bindClick(id: Long) {
            itemView.setOnClickListener {
                categoryPresenter.onCategorySelected(id)
            }
        }
    }
}