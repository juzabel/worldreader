package net.juzabel.worldreader.presentation.base

import android.support.v7.app.AppCompatActivity
import net.juzabel.worldreader.common.WorldreaderApplication
import net.juzabel.worldreader.di.components.AppComponent

object ActivityExtensions {
    fun AppCompatActivity.getAppComponent() : AppComponent = (application as WorldreaderApplication).getApplicationComponent()
}