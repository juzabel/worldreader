package net.juzabel.worldreader.presentation

import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import net.juzabel.worldreader.R
import net.juzabel.worldreader.presentation.view.BookFragment
import net.juzabel.worldreader.presentation.view.CategoryFragment
import javax.inject.Inject

class Navigator @Inject constructor(private val activity: AppCompatActivity){

    fun loadCategoriesFragment() {
        activity.supportFragmentManager.beginTransaction()
            .replace(R.id.flayFragmentContainer, CategoryFragment.newInstance()).commit()
    }

    fun loadBooksFragment(category: Long){
        activity.supportFragmentManager.beginTransaction()
            .replace(R.id.flayFragmentContainer, BookFragment.newInstance(category))
            .addToBackStack(null)
            .commit()
    }

    fun showMessage(message: String){
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
    }

}