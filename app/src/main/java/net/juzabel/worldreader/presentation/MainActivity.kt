package net.juzabel.worldreader.presentation

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import net.juzabel.worldreader.R
import net.juzabel.worldreader.di.components.ActivityComponent
import net.juzabel.worldreader.di.modules.ActivityModule
import net.juzabel.worldreader.presentation.base.ActivityExtensions.getAppComponent
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    private lateinit var activityComponent: ActivityComponent

    @Inject
    lateinit var navigator: Navigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    private fun injectActivityComponent() {
        activityComponent = getAppComponent().plusActivityComponent(ActivityModule(this))
        activityComponent.inject(this)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        injectActivityComponent()
        navigator.loadCategoriesFragment()
    }

    fun getActivityComponent(): ActivityComponent = activityComponent
}
