package net.juzabel.worldreader.presentation.model.entity

data class Book (
    val author: String?,
    val title: String?,
    val cover: String?
)