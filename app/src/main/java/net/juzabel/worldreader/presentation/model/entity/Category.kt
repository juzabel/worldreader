package net.juzabel.worldreader.presentation.model.entity

data class Category(
    val id: Long,
    val title: String,
    val icon: String
)