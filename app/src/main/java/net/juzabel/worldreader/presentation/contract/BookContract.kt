package net.juzabel.worldreader.presentation.contract

import net.juzabel.worldreader.presentation.model.entity.Book

interface BookContract {
    interface View {
        fun onBooksResult(categories: List<Book>?)
    }

    interface Presenter {
        fun getBooks(category: Long?)
    }
}