package net.juzabel.worldreader.presentation.contract

import net.juzabel.worldreader.presentation.model.entity.Category

interface CategoryContract {
    interface View {
        fun onCategoriesResult(categories: List<Category>?)
    }

    interface Presenter {
        fun getCategories()
        fun onCategorySelected(category: Long)
    }
}