package net.juzabel.worldreader.presentation.base

import android.support.v4.app.Fragment
import net.juzabel.worldreader.di.components.ActivityComponent
import net.juzabel.worldreader.presentation.MainActivity

object FragmentExtensions {
    fun Fragment.getActivityComponent() : ActivityComponent = (activity as MainActivity).getActivityComponent()
}