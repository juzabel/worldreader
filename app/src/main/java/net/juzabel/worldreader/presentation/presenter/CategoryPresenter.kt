package net.juzabel.worldreader.presentation.presenter

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import net.juzabel.worldreader.domain.base.UseCase
import net.juzabel.worldreader.domain.entity.CategoryEntity
import net.juzabel.worldreader.presentation.Navigator
import net.juzabel.worldreader.presentation.base.PresenterDisposableManager
import net.juzabel.worldreader.presentation.contract.CategoryContract
import net.juzabel.worldreader.presentation.model.mapper.CategoryMapper
import timber.log.Timber
import javax.inject.Inject

class CategoryPresenter @Inject constructor(
    private val useCase: UseCase<Void, Single<List<CategoryEntity>>>,
    private val categoryMapper: CategoryMapper,
    private val navigator: Navigator
) : PresenterDisposableManager<CategoryContract.View>(), CategoryContract.Presenter {
    override fun getCategories() {

        useCase.execute(null)
            .flattenAsFlowable { it -> it }
            .map { categoryMapper.map(it) }
            .toList()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = {
                    view?.onCategoriesResult(it)
                },
                onError = {
                    navigator.showMessage(it.localizedMessage)
                    view?.onCategoriesResult(null)
                    Timber.e(it)
                })
            .addTo(compositeDisposable)
    }

    override fun onCategorySelected(category: Long) {
        navigator.loadBooksFragment(category)
    }
}