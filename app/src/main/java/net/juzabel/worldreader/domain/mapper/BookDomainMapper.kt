package net.juzabel.worldreader.domain.mapper

import net.juzabel.worldreader.data.db.entity.Book
import net.juzabel.worldreader.domain.entity.BookEntity
import javax.inject.Inject

class BookDomainMapper @Inject constructor() {
    fun map(book: Book): BookEntity = BookEntity(book.author, book.title, book.cover)
}