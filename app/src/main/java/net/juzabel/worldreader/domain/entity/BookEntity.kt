package net.juzabel.worldreader.domain.entity

data class BookEntity (
    val author: String?,
    val title: String?,
    val cover: String?
)