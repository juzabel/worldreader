package net.juzabel.worldreader.domain.entity

data class CategoryEntity(
    val id: Long,
    val title: String,
    val icon: String
)