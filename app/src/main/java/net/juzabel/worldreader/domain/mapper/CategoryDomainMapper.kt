package net.juzabel.worldreader.domain.mapper

import net.juzabel.worldreader.data.db.entity.Category
import net.juzabel.worldreader.domain.entity.CategoryEntity
import javax.inject.Inject

class CategoryDomainMapper @Inject constructor() {
    fun map(category: Category): CategoryEntity = CategoryEntity(category.id, category.title, category.icon)
}