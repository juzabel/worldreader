package net.juzabel.worldreader.domain.interactor

import io.reactivex.Single
import net.juzabel.worldreader.data.repository.BookRepository
import net.juzabel.worldreader.domain.base.UseCase
import net.juzabel.worldreader.domain.entity.BookEntity
import net.juzabel.worldreader.domain.mapper.BookDomainMapper
import javax.inject.Inject

class BookInteractor @Inject constructor(
    private val bookRepository: BookRepository,
    private val bookDomainMapper: BookDomainMapper
) : @Inject UseCase<Long, Single<List<BookEntity>>>() {

    override fun buildUseCase(params: Long?): Single<List<BookEntity>> {
        return if (params != null) {
            bookRepository.getBookByCategory(params).flattenAsObservable { book -> book }
                .map { book -> bookDomainMapper.map(book) }
                .toList()
        } else {
            Single.just(listOf())
        }
    }
}