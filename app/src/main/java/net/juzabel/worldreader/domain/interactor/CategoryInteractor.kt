package net.juzabel.worldreader.domain.interactor

import io.reactivex.Single
import net.juzabel.worldreader.data.repository.CategoryRepository
import net.juzabel.worldreader.domain.base.UseCase
import net.juzabel.worldreader.domain.entity.CategoryEntity
import net.juzabel.worldreader.domain.mapper.CategoryDomainMapper
import javax.inject.Inject

class CategoryInteractor @Inject constructor(
    private val categoryRepository: CategoryRepository,
    private val categoryDomainMapper: CategoryDomainMapper
) : UseCase<Void, Single<List<CategoryEntity>>>() {

    override fun buildUseCase(params: Void?): Single<List<CategoryEntity>> =
        categoryRepository.getCategories().flattenAsObservable { it -> it }.map { categoryDomainMapper.map(it) }.toList()

}