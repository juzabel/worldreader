package net.juzabel.worldreader.domain.base

abstract class UseCase<P, T>{
    fun execute( params: P?) = buildUseCase(params)

    abstract fun buildUseCase(params: P?): T
}