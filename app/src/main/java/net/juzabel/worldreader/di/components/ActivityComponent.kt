package net.juzabel.worldreader.di.components

import android.support.v7.app.AppCompatActivity
import dagger.Subcomponent
import net.juzabel.worldreader.di.modules.ActivityModule
import net.juzabel.worldreader.di.modules.FragmentModule
import net.juzabel.worldreader.presentation.MainActivity

@Subcomponent(modules = arrayOf(ActivityModule::class))
interface ActivityComponent {
    fun inject(mainActivity: MainActivity)

    fun provideActivity(): AppCompatActivity

    fun plusFragmentComponent(fragmentModule: FragmentModule): FragmentComponent

}