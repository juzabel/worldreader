package net.juzabel.worldreader.di.components

import dagger.Component
import net.juzabel.worldreader.common.HashCalculator
import net.juzabel.worldreader.common.WorldreaderApplication
import net.juzabel.worldreader.di.modules.ActivityModule
import net.juzabel.worldreader.di.modules.AppModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {
    fun inject(application: WorldreaderApplication)
    fun plusActivityComponent(activityModule: ActivityModule) : ActivityComponent
    fun provideHashCalculator(): HashCalculator
}