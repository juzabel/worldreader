package net.juzabel.worldreader.di.modules

import android.arch.persistence.room.Room
import android.content.Context
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import net.juzabel.worldreader.BuildConfig
import net.juzabel.worldreader.common.HashCalculator
import net.juzabel.worldreader.common.WorldreaderApplication
import net.juzabel.worldreader.data.db.Database
import net.juzabel.worldreader.data.network.Api
import net.juzabel.worldreader.data.network.HeadersInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton


@Module
class AppModule(private val application: WorldreaderApplication) {

    @Provides
    @Singleton
    fun provideApi(): Api {
        val builder: OkHttpClient.Builder = OkHttpClient.Builder()

        if (BuildConfig.BUILD_TYPE.equals("debug", true)) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(interceptor)
                .addInterceptor(HeadersInterceptor(provideHashCalculator()))
        }

        val retrofit: Retrofit = Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(builder.build()).build()
        return retrofit.create(Api::class.java)
    }

    @Provides
    @Singleton
    fun provideDatabase(): Database {
        return Room.databaseBuilder(application as Context, Database::class.java, Database.DB_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideHashCalculator(): HashCalculator = HashCalculator()

    @Provides
    fun provideApp(): WorldreaderApplication = application

    @Provides
    @Named("base_image")
    fun provideBaseImage() : String = BuildConfig.IMAGE_URL

    fun provideWorldreaderApplication(): WorldreaderApplication = application
}