package net.juzabel.worldreader.di.modules

import android.support.v7.app.AppCompatActivity
import dagger.Module
import dagger.Provides
import net.juzabel.worldreader.presentation.MainActivity


@Module
class ActivityModule(private val activity: MainActivity) {

    @Provides
    fun provideActivity(): AppCompatActivity = activity
}