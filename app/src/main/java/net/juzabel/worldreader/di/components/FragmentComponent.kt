package net.juzabel.worldreader.di.components

import dagger.Subcomponent
import net.juzabel.worldreader.di.modules.FragmentModule
import net.juzabel.worldreader.presentation.view.BookFragment
import net.juzabel.worldreader.presentation.view.CategoryFragment

@Subcomponent(modules = arrayOf(FragmentModule::class))
interface FragmentComponent {
    fun inject(categoryFragment: CategoryFragment)
    fun inject(bookFragment: BookFragment)
}