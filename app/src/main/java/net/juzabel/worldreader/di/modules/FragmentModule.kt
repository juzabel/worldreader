package net.juzabel.worldreader.di.modules

import dagger.Module
import dagger.Provides
import io.reactivex.Single
import net.juzabel.worldreader.data.repository.BookRepository
import net.juzabel.worldreader.data.repository.BookRepositoryImpl
import net.juzabel.worldreader.data.repository.CategoryRepository
import net.juzabel.worldreader.data.repository.CategoryRepositoryImpl
import net.juzabel.worldreader.domain.base.UseCase
import net.juzabel.worldreader.domain.entity.BookEntity
import net.juzabel.worldreader.domain.entity.CategoryEntity
import net.juzabel.worldreader.domain.interactor.BookInteractor
import net.juzabel.worldreader.domain.interactor.CategoryInteractor


@Module
class FragmentModule {

    @Provides
    fun getCategoryInteractor(categoryInteractor: CategoryInteractor): UseCase<Void, Single<List<CategoryEntity>>> =
        categoryInteractor

    @Provides
    fun getBookInteractor(bookInteractor: BookInteractor): UseCase<Long, Single<List<BookEntity>>> =
        bookInteractor

    @Provides
    fun getCategoryRepository(categoryRepositoryImpl: CategoryRepositoryImpl): CategoryRepository = categoryRepositoryImpl

    @Provides
    fun getBookRepository(bookRepositoryImpl: BookRepositoryImpl): BookRepository = bookRepositoryImpl
}