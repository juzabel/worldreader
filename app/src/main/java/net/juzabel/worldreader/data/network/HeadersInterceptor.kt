package net.juzabel.worldreader.data.network

import net.juzabel.worldreader.BuildConfig
import net.juzabel.worldreader.common.HashCalculator
import okhttp3.Interceptor
import okhttp3.Response
import java.util.*
import javax.inject.Inject

class HeadersInterceptor @Inject constructor(val hashCalculator: HashCalculator): Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        val timestamp = Date().time

        val hash = hashCalculator.calculate(request.url().toString(), timestamp, BuildConfig.TOKEN)

        request = request.newBuilder()
            .addHeader(BuildConfig.HEADER_CLIENT, BuildConfig.HEADER_CLIENT_VALUE)
            .addHeader(BuildConfig.HEADER_HASH, hash)
            .addHeader(BuildConfig.HEADER_TIMESTAMP, timestamp.toString())
            .build()
        return chain.proceed(request)
    }

}