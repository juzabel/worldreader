package net.juzabel.worldreader.data.repository.datasource

import io.reactivex.Single
import net.juzabel.worldreader.data.db.entity.Category

interface CategoryDataSource {

    fun getCategories(): Single<List<Category>>
}