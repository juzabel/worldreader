package net.juzabel.worldreader.data.repository.datasource

import io.reactivex.Single
import net.juzabel.worldreader.data.db.entity.Category
import net.juzabel.worldreader.data.mapper.CategoryDataMapper
import net.juzabel.worldreader.data.network.RestService
import javax.inject.Inject

class CategoryNetworkDataSource @Inject constructor(
    private val restService: RestService,
    private val categoryDataMapper: CategoryDataMapper
) : CategoryDataSource {
    override fun getCategories(): Single<List<Category>>
    = restService.getCategories().flattenAsObservable { it -> it }.map { categoryDataMapper.map(it) }.toList()
}