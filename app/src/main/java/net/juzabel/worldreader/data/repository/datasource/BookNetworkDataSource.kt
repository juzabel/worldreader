package net.juzabel.worldreader.data.repository.datasource

import io.reactivex.Single
import net.juzabel.worldreader.data.db.entity.Book
import net.juzabel.worldreader.data.mapper.BookDataMapper
import net.juzabel.worldreader.data.network.RestService
import javax.inject.Inject

class BookNetworkDataSource @Inject constructor(
    private val restService: RestService,
    private val bookDataMapper: BookDataMapper
) : BookDataSource {

    override fun getBooks(category: Long): Single<List<Book>>
    = restService.getBooks(category).flattenAsObservable { it-> it }.map { bookDataMapper.map(it) }.toList()

}