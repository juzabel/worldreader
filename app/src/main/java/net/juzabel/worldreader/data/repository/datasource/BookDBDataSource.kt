package net.juzabel.worldreader.data.repository.datasource

import io.reactivex.Completable
import io.reactivex.Single
import net.juzabel.worldreader.data.db.Database
import net.juzabel.worldreader.data.db.entity.Book
import javax.inject.Inject

class BookDBDataSource @Inject constructor(private val database: Database) : BookDataSource {

    override fun getBooks(category: Long): Single<List<Book>> = database.bookDao().getByCategory(category)

    fun insert(book: Book): Completable = Completable.fromAction { database.bookDao().insert(book) }

    fun insertAll(bookList: List<Book>): Completable = Completable.fromAction { database.bookDao().insertAll(bookList) }
}