package net.juzabel.worldreader.data.network

import io.reactivex.Single
import net.juzabel.worldreader.data.network.entity.BookResponse
import net.juzabel.worldreader.data.network.entity.CategoryResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("/categories/eng")
    fun getCategories() : Single<List<CategoryResponse>>

    @GET("/books?sort=date:desc")
    fun getBooksByCategory(@Query("category") category: Long): Single<List<BookResponse>>
}