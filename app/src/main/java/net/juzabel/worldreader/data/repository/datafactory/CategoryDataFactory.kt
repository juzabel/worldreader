package net.juzabel.worldreader.data.repository.datafactory

import net.juzabel.worldreader.data.db.Database
import net.juzabel.worldreader.data.mapper.CategoryDataMapper
import net.juzabel.worldreader.data.network.RestService
import net.juzabel.worldreader.data.repository.datasource.CategoryDBDataSource
import net.juzabel.worldreader.data.repository.datasource.CategoryNetworkDataSource
import javax.inject.Inject

class CategoryDataFactory @Inject constructor(
    private val database: Database,
    private val restService: RestService,
    private val categoryDataMapper: CategoryDataMapper
) {
    fun createNetworkDataSource(): CategoryNetworkDataSource =
        CategoryNetworkDataSource(restService, categoryDataMapper)

    fun createDBDataSource(): CategoryDBDataSource = CategoryDBDataSource(database)
}