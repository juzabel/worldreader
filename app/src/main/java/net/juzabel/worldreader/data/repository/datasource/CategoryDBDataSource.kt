package net.juzabel.worldreader.data.repository.datasource

import io.reactivex.Completable
import io.reactivex.Single
import net.juzabel.worldreader.data.db.Database
import net.juzabel.worldreader.data.db.entity.Category
import javax.inject.Inject

class CategoryDBDataSource @Inject constructor(private val database: Database) : CategoryDataSource {

    override fun getCategories(): Single<List<Category>> = database.categoryDao().getAll()

    fun insert(category: Category): Completable = Completable.fromAction { database.categoryDao().insert(category) }

    fun insertAll(categoryList: List<Category>): Completable =
        Completable.fromAction { database.categoryDao().insertAll(categoryList) }

}