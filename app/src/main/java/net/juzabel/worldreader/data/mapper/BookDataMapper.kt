package net.juzabel.worldreader.data.mapper

import net.juzabel.worldreader.data.db.entity.Book
import net.juzabel.worldreader.data.network.entity.BookResponse
import javax.inject.Inject

class BookDataMapper @Inject constructor() {

    fun map(bookResponse: BookResponse): Book =
        Book(author = bookResponse.author, title = bookResponse.title, cover = bookResponse.cover)
}