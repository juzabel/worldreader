package net.juzabel.worldreader.data.network

import io.reactivex.Single
import net.juzabel.worldreader.data.network.entity.BookResponse
import net.juzabel.worldreader.data.network.entity.CategoryResponse
import javax.inject.Inject

class RestService @Inject constructor(private val api: Api) {
    fun getCategories(): Single<List<CategoryResponse>> = api.getCategories()
    fun getBooks(category: Long): Single<List<BookResponse>> = api.getBooksByCategory(category)
}