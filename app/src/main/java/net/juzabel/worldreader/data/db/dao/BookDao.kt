package net.juzabel.worldreader.data.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import io.reactivex.Single
import net.juzabel.worldreader.data.db.entity.Book

@Dao
interface BookDao {
    @Insert
    fun insert(book: Book): Long

    @Insert
    fun insertAll(bookList: List<Book>): List<Long>

    @Query("SELECT * FROM book WHERE categoryId = :category")
    fun getByCategory(category: Long): Single<List<Book>>

    @Delete
    fun delete(book: Book)
}