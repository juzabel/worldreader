package net.juzabel.worldreader.data.network.entity

data class BookResponse(
    val author: String,
    val title: String,
    val cover: String
)