package net.juzabel.worldreader.data.repository.datafactory

import net.juzabel.worldreader.data.db.Database
import net.juzabel.worldreader.data.mapper.BookDataMapper
import net.juzabel.worldreader.data.network.RestService
import net.juzabel.worldreader.data.repository.datasource.BookDBDataSource
import net.juzabel.worldreader.data.repository.datasource.BookNetworkDataSource
import javax.inject.Inject

class BookDataFactory @Inject constructor(
    private val database: Database,
    private val restService: RestService,
    private val bookDataMapper: BookDataMapper
) {
    fun createNetworkDataSource(): BookNetworkDataSource = BookNetworkDataSource(restService, bookDataMapper)

    fun createDBDataSource(): BookDBDataSource = BookDBDataSource(database)
}