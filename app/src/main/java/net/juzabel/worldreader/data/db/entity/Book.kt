package net.juzabel.worldreader.data.db.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.ForeignKey.CASCADE
import android.arch.persistence.room.PrimaryKey

@Entity(
    tableName = "book", foreignKeys = arrayOf(
        ForeignKey(
            entity = Category::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("categoryId"),
            onDelete = CASCADE
        )
    )
)
data class Book(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    val author: String?,
    val title: String?,
    val cover: String?,
    var categoryId: Long = 0
)