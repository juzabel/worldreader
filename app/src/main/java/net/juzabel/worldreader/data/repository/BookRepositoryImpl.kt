package net.juzabel.worldreader.data.repository

import io.reactivex.Observable
import io.reactivex.Single
import net.juzabel.worldreader.data.db.entity.Book
import net.juzabel.worldreader.data.repository.datafactory.BookDataFactory
import javax.inject.Inject

class BookRepositoryImpl @Inject constructor(private val bookDataFactory: BookDataFactory) : BookRepository {
    override fun getBookByCategory(category: Long): Single<List<Book>> {

        return Single.defer {
            bookDataFactory.createDBDataSource().getBooks(category)
                .flatMap {
                    if (it.isEmpty()) {
                        return@flatMap fromServer(category)
                    } else {
                        return@flatMap Single.just(it)
                    }
                }
                .onErrorResumeNext {
                    return@onErrorResumeNext fromServer(category)
                }
        }
    }

    private fun fromServer(category: Long): Single<List<Book>>? {
        return Single.defer {
            bookDataFactory.createNetworkDataSource().getBooks(category)
                .flattenAsObservable { bookList -> bookList }
                .flatMap { book ->
                    book.categoryId = category
                    bookDataFactory.createDBDataSource()
                        .insert(book)
                        .andThen(Observable.just(book))
                }.toList()
        }
    }

}