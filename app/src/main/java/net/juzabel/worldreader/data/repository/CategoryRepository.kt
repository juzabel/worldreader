package net.juzabel.worldreader.data.repository

import io.reactivex.Single
import net.juzabel.worldreader.data.db.entity.Category

interface CategoryRepository {
    fun getCategories() : Single<List<Category>>
}