package net.juzabel.worldreader.data.network.entity

data class CategoryResponse(
    val id: Long,
    val title: String,
    val icon: String
)