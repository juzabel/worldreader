package net.juzabel.worldreader.data.mapper

import net.juzabel.worldreader.data.db.entity.Category
import net.juzabel.worldreader.data.network.entity.CategoryResponse
import javax.inject.Inject

class CategoryDataMapper @Inject constructor() {
    fun map(categoryResponse: CategoryResponse): Category = Category(categoryResponse.id, categoryResponse.title, categoryResponse.icon)
}