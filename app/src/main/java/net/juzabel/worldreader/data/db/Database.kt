package net.juzabel.worldreader.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import net.juzabel.worldreader.data.db.dao.BookDao
import net.juzabel.worldreader.data.db.dao.CategoryDao
import net.juzabel.worldreader.data.db.entity.Book
import net.juzabel.worldreader.data.db.entity.Category


@Database(entities = arrayOf(Category::class, Book::class), version = 1, exportSchema = false)
abstract class Database : RoomDatabase() {
    companion object {
        const val DB_NAME = "worldreader.db"
    }
    abstract fun categoryDao(): CategoryDao
    abstract fun bookDao(): BookDao
}