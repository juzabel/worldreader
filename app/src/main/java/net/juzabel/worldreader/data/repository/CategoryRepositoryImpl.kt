package net.juzabel.worldreader.data.repository

import io.reactivex.Single
import net.juzabel.worldreader.data.db.entity.Category
import net.juzabel.worldreader.data.repository.datafactory.CategoryDataFactory
import javax.inject.Inject

class CategoryRepositoryImpl @Inject constructor(private val categoryDataFactory: CategoryDataFactory) :
    CategoryRepository {
    override fun getCategories(): Single<List<Category>> {
        return categoryDataFactory.createDBDataSource().getCategories()
            .flatMap {
                if(it.isEmpty()){
                    return@flatMap fromServer()
                }else {
                    return@flatMap Single.just(it)
                }
            }
            .onErrorResumeNext {
                return@onErrorResumeNext fromServer()
            }
    }

    private fun fromServer(): Single<List<Category>> {
        return categoryDataFactory.createNetworkDataSource().getCategories()
            .flatMap { categoryList ->
                categoryDataFactory.createDBDataSource().insertAll(categoryList).toSingleDefault(categoryList)
            }
    }
}