package net.juzabel.worldreader.data.repository.datasource

import io.reactivex.Single
import net.juzabel.worldreader.data.db.entity.Book

interface BookDataSource {

    fun getBooks(category: Long): Single<List<Book>>
}