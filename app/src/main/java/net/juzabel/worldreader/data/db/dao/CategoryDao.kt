package net.juzabel.worldreader.data.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import io.reactivex.Single
import net.juzabel.worldreader.data.db.entity.Category

@Dao
interface CategoryDao {

    @Insert
    fun insert(category: Category): Long

    @Insert
    fun insertAll(categoryList: List<Category>): List<Long>

    @Query("SELECT * FROM category")
    fun getAll(): Single<List<Category>>

    @Delete
    fun delete(category: Category)
}