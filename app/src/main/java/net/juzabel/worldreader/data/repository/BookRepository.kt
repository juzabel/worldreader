package net.juzabel.worldreader.data.repository

import io.reactivex.Single
import net.juzabel.worldreader.data.db.entity.Book

interface BookRepository {
    fun getBookByCategory(category: Long): Single<List<Book>>
}