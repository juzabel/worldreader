package net.juzabel.worldreader.androidtest

import android.support.test.espresso.Espresso
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import net.juzabel.worldreader.presentation.MainActivity
import net.juzabel.worldreader.presentation.view.adapter.CategoriesAdapter
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnit
import net.juzabel.worldreader.R


@RunWith(AndroidJUnit4::class)
@LargeTest
class MainActivityInstrumentedTest {

    @Rule
    @JvmField
    val rule = ActivityTestRule<MainActivity>(MainActivity::class.java, true, true)

    @Rule
    @JvmField
    var ruleMockito = MockitoJUnit.rule()

    @Test
    fun mainActivityTest() {

        Espresso.onView(ViewMatchers.withId(R.id.rvCategories))
            .perform(RecyclerViewActions.scrollToPosition<CategoriesAdapter.ViewHolder>(0))
        Espresso.onView(ViewMatchers.withText("Category1"))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withText("Category1"))
            .perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withText("Author1"))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.pressBack()

        Espresso.onView(ViewMatchers.withId(R.id.rvCategories))
            .perform(RecyclerViewActions.scrollToPosition<CategoriesAdapter.ViewHolder>(1))
        Espresso.onView(ViewMatchers.withText("Critics' Picks"))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withText("Critics' Picks"))
            .perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withText("Jonathan Swift"))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.pressBack()

        Espresso.onView(ViewMatchers.withId(R.id.rvCategories))
            .perform(RecyclerViewActions.scrollToPosition<CategoriesAdapter.ViewHolder>(2))
        Espresso.onView(ViewMatchers.withText("Love"))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withText("Love"))
            .perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withText("Irenosen Okojie"))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

}