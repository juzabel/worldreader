package net.juzabel.worldreader

import android.app.Application
import android.content.Context
import com.nhaarman.mockitokotlin2.whenever
import net.juzabel.worldreader.common.HashCalculator
import net.juzabel.worldreader.common.WorldreaderApplication
import net.juzabel.worldreader.data.db.Database
import net.juzabel.worldreader.di.components.AppComponent
import net.juzabel.worldreader.di.components.DaggerAppComponent
import net.juzabel.worldreader.di.modules.AppModule
import net.juzabel.worldreader.mockdb.MockBookDao
import net.juzabel.worldreader.mockdb.MockCategoryDao
import net.juzabel.worldreader.mockdb.MockDatabase
import org.mockito.Mockito

class MockApplication() : WorldreaderApplication, Application() {

    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        injectApplicationComponent()
    }

    override fun injectApplicationComponent() {
        appComponent = DaggerAppComponent.builder().appModule(getApplicationModule()).build()
        appComponent.inject(this)

    }

    override fun getApplicationComponent(): AppComponent = appComponent

    override fun getContext(): Context = this

    override fun getApplicationModule(): AppModule? {
        val mockModule = Mockito.mock(AppModule::class.java)
        whenever(mockModule.provideApi()).thenReturn(MockApi())
        whenever(mockModule.provideDatabase()).thenReturn(getMockDatabase())
        whenever(mockModule.provideApp()).thenReturn(this)
        whenever(mockModule.provideWorldreaderApplication()).thenReturn(this)
        whenever(mockModule.provideHashCalculator()).thenReturn(HashCalculator())
        whenever(mockModule.provideBaseImage()).thenReturn("")
        return mockModule
    }

    private fun getMockDatabase(): Database? {
        return MockDatabase(MockBookDao(), MockCategoryDao())
    }
}