package net.juzabel.worldreader

import android.support.test.InstrumentationRegistry
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import io.reactivex.Single
import net.juzabel.worldreader.data.network.Api
import net.juzabel.worldreader.data.network.entity.BookResponse
import net.juzabel.worldreader.data.network.entity.CategoryResponse
import timber.log.Timber
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.lang.reflect.Type

class MockApi : Api {
    val gson: Gson = Gson()

    override fun getCategories(): Single<List<CategoryResponse>> {
        val typeCategoryList: Type = object : TypeToken<List<CategoryResponse>>() {}.type

        return Single.just(
            gson.fromJson(
                getJson("categories.json"), typeCategoryList
            )
        )
    }

    override fun getBooksByCategory(category: Long): Single<List<BookResponse>> {
        val typeBookList: Type = object : TypeToken<List<BookResponse>>() {}.type

        return Single.just(
            gson.fromJson(
                getJson("books/" + category.toString() + ".json"), typeBookList
            )
        )

    }

    fun getJson(filePath: String): String {
        var ret = ""
        try {
            val stream = InstrumentationRegistry.getInstrumentation().context.resources.assets.open(filePath)

            ret = convertStreamToString(stream)
            //Make sure you close all streams.
            stream.close()
        } catch (e: Exception) {
            Timber.e(e)
        }
        return ret
    }

    private fun convertStreamToString(inputStream: InputStream): String {
        val reader = BufferedReader(InputStreamReader(inputStream))
        val sb = StringBuilder()
        var line: String? = reader.readLine()
        while (line != null) {
            sb.append(line).append("\n")
            line = reader.readLine()
        }
        reader.close()
        return sb.toString()
    }
}