package net.juzabel.worldreader.mockdb

import io.reactivex.Single
import net.juzabel.worldreader.data.db.dao.BookDao
import net.juzabel.worldreader.data.db.entity.Book

class MockBookDao : BookDao {
    override fun insertAll(bookList: List<Book>): List<Long> = arrayListOf()

    override fun getByCategory(category: Long): Single<List<Book>> = Single.just(arrayListOf())

    override fun delete(book: Book) {}

    override fun insert(book: Book): Long = 1L

}