package net.juzabel.worldreader.mockdb

import android.arch.persistence.db.SupportSQLiteOpenHelper
import android.arch.persistence.room.DatabaseConfiguration
import android.arch.persistence.room.InvalidationTracker
import net.juzabel.worldreader.data.db.Database
import net.juzabel.worldreader.data.db.dao.BookDao
import net.juzabel.worldreader.data.db.dao.CategoryDao

class MockDatabase(val bookDao: BookDao, val categoryDao: CategoryDao) : Database() {

    override fun categoryDao(): CategoryDao = categoryDao

    override fun bookDao(): BookDao = bookDao

    override fun createOpenHelper(config: DatabaseConfiguration?): SupportSQLiteOpenHelper = openHelper

    override fun createInvalidationTracker(): InvalidationTracker = InvalidationTracker(this)

    override fun clearAllTables() {}

}