package net.juzabel.worldreader.mockdb

import io.reactivex.Single
import net.juzabel.worldreader.data.db.dao.CategoryDao
import net.juzabel.worldreader.data.db.entity.Category

class MockCategoryDao : CategoryDao {
    override fun insert(category: Category): Long = 1L

    override fun insertAll(categoryList: List<Category>): List<Long> = arrayListOf()

    override fun getAll(): Single<List<Category>> = Single.just(arrayListOf())

    override fun delete(category: Category) {}
}