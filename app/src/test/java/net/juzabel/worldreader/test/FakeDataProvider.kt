package net.juzabel.worldreader.test

import net.juzabel.worldreader.data.db.entity.Book
import net.juzabel.worldreader.data.db.entity.Category
import net.juzabel.worldreader.data.network.entity.BookResponse
import net.juzabel.worldreader.domain.entity.BookEntity
import net.juzabel.worldreader.domain.entity.CategoryEntity

object FakeDataProvider {

    fun getCategoryEntities(): List<CategoryEntity> {
        val listCategoryEntity = arrayListOf<CategoryEntity>()
        listCategoryEntity.add(CategoryEntity(1, "title1", "icon1"))
        listCategoryEntity.add(CategoryEntity(2, "title2", "icon2"))

        return listCategoryEntity
    }

    fun getBookEntities(): List<BookEntity> {
        val listBookEntity = arrayListOf<BookEntity>()
        listBookEntity.add(BookEntity("author1", "title1", "cover1"))
        listBookEntity.add(BookEntity("author2", "title2", "cover2"))

        return listBookEntity
    }

    fun getDataCategories(): List<Category> {
        val listCategories = arrayListOf<Category>()

        listCategories.add(Category(1, "title1", "icon1"))
        listCategories.add(Category(2, "title2", "icon2"))

        return listCategories
    }

    fun getDataBooks(id: Long): List<Book> {
        val listBook = arrayListOf<Book>()

        listBook.add(Book(id,"author1", "title1", "cover1"))
        listBook.add(Book(id, "author2", "title2", "cover2"))

        return listBook
    }

    fun getResponseBooks1(): List<BookResponse> {
        val listBook = arrayListOf<BookResponse>()

        listBook.add(BookResponse("author1", "title1", "cover1"))
        listBook.add(BookResponse( "author2", "title2", "cover2"))

        return listBook
    }

}