package net.juzabel.worldreader.test.interactor

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import junit.framework.Assert.assertTrue
import net.juzabel.worldreader.data.repository.BookRepository
import net.juzabel.worldreader.domain.entity.BookEntity
import net.juzabel.worldreader.domain.interactor.BookInteractor
import net.juzabel.worldreader.domain.mapper.BookDomainMapper
import net.juzabel.worldreader.test.FakeDataProvider
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class BookInteractorTest {

    @Mock
    lateinit var bookRepository: BookRepository

    lateinit var bookDomainMapper: BookDomainMapper

    lateinit var interactor: BookInteractor

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        bookDomainMapper = BookDomainMapper()

        interactor = BookInteractor(bookRepository, bookDomainMapper)
    }

    @Test
    fun testGetBookDomain() {
        val resultBooks = FakeDataProvider.getDataBooks(1)

        whenever(bookRepository.getBookByCategory(any())).thenReturn(Single.just(resultBooks))

        val testObserver: TestObserver<List<BookEntity>> = interactor.execute(1).test()
        testObserver.assertNoErrors()
        testObserver.assertComplete()

        val listBookEntity = testObserver.events[0][0] as List<BookEntity>

        assertTrue(listBookEntity.size == resultBooks.size)
        assertTrue(listBookEntity[0].author == resultBooks[0].author)
        assertTrue(listBookEntity[0].title == resultBooks[0].title)
        assertTrue(listBookEntity[0].cover == resultBooks[0].cover)
        assertTrue(listBookEntity[1].author == resultBooks[1].author)
        assertTrue(listBookEntity[1].title == resultBooks[1].title)
        assertTrue(listBookEntity[1].cover == resultBooks[1].cover)
    }

}