package net.juzabel.worldreader.test.interactor

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import junit.framework.Assert.assertTrue
import net.juzabel.worldreader.data.db.entity.Category
import net.juzabel.worldreader.data.repository.CategoryRepositoryImpl
import net.juzabel.worldreader.data.repository.datafactory.CategoryDataFactory
import net.juzabel.worldreader.data.repository.datasource.CategoryDBDataSource
import net.juzabel.worldreader.data.repository.datasource.CategoryNetworkDataSource
import net.juzabel.worldreader.test.FakeDataProvider
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class CategoryRepositoryTest {

    @Mock
    lateinit var categoryDataDBDataSource: CategoryDBDataSource

    @Mock
    lateinit var categoryDataNetworkDataSource: CategoryNetworkDataSource

    @Mock
    lateinit var categoryDataFactory: CategoryDataFactory

    lateinit var categoryRepository: CategoryRepositoryImpl

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        whenever(categoryDataFactory.createDBDataSource()).thenReturn(categoryDataDBDataSource)
        whenever(categoryDataFactory.createNetworkDataSource()).thenReturn(categoryDataNetworkDataSource)

        categoryRepository = CategoryRepositoryImpl(categoryDataFactory)
    }

    @Test
    fun testGetBookDataNW() {
        val resultCategories1 = FakeDataProvider.getDataCategories()

        whenever(categoryDataDBDataSource.getCategories()).thenReturn(Single.just(arrayListOf()))
        whenever(categoryDataDBDataSource.insertAll(any())).thenReturn(Completable.complete())
        whenever(categoryDataNetworkDataSource.getCategories()).thenReturn(Single.just(resultCategories1))

        val testObserver: TestObserver<List<Category>> = categoryRepository.getCategories().test()
        testObserver.assertNoErrors()
        testObserver.assertComplete()

        val listBookEntity = testObserver.events[0][0] as List<Category>

        assertTrue(listBookEntity.size == resultCategories1.size)
        assertTrue(listBookEntity[0].icon == resultCategories1[0].icon)
        assertTrue(listBookEntity[0].title == resultCategories1[0].title)
        assertTrue(listBookEntity[0].id == resultCategories1[0].id)
        assertTrue(listBookEntity[1].icon == resultCategories1[1].icon)
        assertTrue(listBookEntity[1].title == resultCategories1[1].title)
        assertTrue(listBookEntity[1].id == resultCategories1[1].id)
    }

    @Test
    fun testGetBookDataDB() {
        val resultCategories1 = FakeDataProvider.getDataCategories()

        whenever(categoryDataDBDataSource.getCategories()).thenReturn(Single.just(resultCategories1))
        whenever(categoryDataDBDataSource.insertAll(any())).thenReturn(Completable.complete())
        whenever(categoryDataNetworkDataSource.getCategories()).thenReturn(Single.just(arrayListOf()))

        val testObserver: TestObserver<List<Category>> = categoryRepository.getCategories().test()
        testObserver.assertNoErrors()
        testObserver.assertComplete()

        val listBookEntity = testObserver.events[0][0] as List<Category>

        assertTrue(listBookEntity.size == resultCategories1.size)
        assertTrue(listBookEntity[0].icon == resultCategories1[0].icon)
        assertTrue(listBookEntity[0].title == resultCategories1[0].title)
        assertTrue(listBookEntity[0].id == resultCategories1[0].id)
        assertTrue(listBookEntity[1].icon == resultCategories1[1].icon)
        assertTrue(listBookEntity[1].title == resultCategories1[1].title)
        assertTrue(listBookEntity[1].id == resultCategories1[1].id)
    }
}