package net.juzabel.worldreader.test.presenter

import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import junit.framework.Assert.assertTrue
import net.juzabel.worldreader.domain.base.UseCase
import net.juzabel.worldreader.domain.entity.CategoryEntity
import net.juzabel.worldreader.presentation.Navigator
import net.juzabel.worldreader.presentation.contract.CategoryContract
import net.juzabel.worldreader.presentation.model.entity.Category
import net.juzabel.worldreader.presentation.model.mapper.CategoryMapper
import net.juzabel.worldreader.presentation.presenter.CategoryPresenter
import net.juzabel.worldreader.test.FakeDataProvider
import net.juzabel.worldreader.test.base.BaseTest
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class CategoryPresenterTest : BaseTest() {

    @Mock
    lateinit var navigator: Navigator
    @Mock
    lateinit var usecase: UseCase<Void, Single<List<CategoryEntity>>>
    @Mock
    lateinit var view: CategoryContract.View

    private lateinit var categoryMapper: CategoryMapper
    private lateinit var categoryPresenter: CategoryPresenter

    @Captor
    lateinit var captor: ArgumentCaptor<List<Category>>

    @Before
    fun setup() {

        MockitoAnnotations.initMocks(this)

        categoryMapper = CategoryMapper()
        categoryPresenter = CategoryPresenter(usecase, categoryMapper, navigator)
        categoryPresenter.view = view
    }

    @Test
    fun testGetCategories() {

        val interactorValues = FakeDataProvider.getCategoryEntities()
        whenever(usecase.execute(null)).thenReturn(Single.just(interactorValues))

        categoryPresenter.getCategories()
        verify(view).onCategoriesResult(captor.capture())

        val result: List<Category> = captor.value
        assertTrue(result.size == interactorValues.size)
        assertTrue(result[0].icon == interactorValues[0].icon)
        assertTrue(result[0].title == interactorValues[0].title)
        assertTrue(result[0].id == interactorValues[0].id)
        assertTrue(result[1].icon == interactorValues[1].icon)
        assertTrue(result[1].title == interactorValues[1].title)
        assertTrue(result[1].id == interactorValues[1].id)

    }
}