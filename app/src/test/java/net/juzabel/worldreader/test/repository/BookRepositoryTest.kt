package net.juzabel.worldreader.test.interactor

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import junit.framework.Assert.assertTrue
import net.juzabel.worldreader.data.db.entity.Book
import net.juzabel.worldreader.data.repository.BookRepositoryImpl
import net.juzabel.worldreader.data.repository.datafactory.BookDataFactory
import net.juzabel.worldreader.data.repository.datasource.BookDBDataSource
import net.juzabel.worldreader.data.repository.datasource.BookNetworkDataSource
import net.juzabel.worldreader.test.FakeDataProvider
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class BookRepositoryTest {

    @Mock
    lateinit var bookDataDBDataSource: BookDBDataSource

    @Mock
    lateinit var bookDataNetworkDataSource: BookNetworkDataSource

    @Mock
    lateinit var bookDataFactory: BookDataFactory

    lateinit var bookRepository: BookRepositoryImpl

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        whenever(bookDataFactory.createDBDataSource()).thenReturn(bookDataDBDataSource)
        whenever(bookDataFactory.createNetworkDataSource()).thenReturn(bookDataNetworkDataSource)

        bookRepository = BookRepositoryImpl(bookDataFactory)
    }

    @Test
    fun testGetBookDataNW() {
        val resultBooks1 = FakeDataProvider.getDataBooks(1)

        whenever(bookDataDBDataSource.getBooks(1)).thenReturn(Single.just(arrayListOf()))
        whenever(bookDataDBDataSource.insert(any())).thenReturn(Completable.complete())
        whenever(bookDataNetworkDataSource.getBooks(1)).thenReturn(Single.just(resultBooks1))

        val testObserver: TestObserver<List<Book>> = bookRepository.getBookByCategory(1).test()
        testObserver.assertNoErrors()
        testObserver.assertComplete()

        val listBookEntity = testObserver.events[0][0] as List<Book>

        assertTrue(listBookEntity.size == resultBooks1.size)
        assertTrue(listBookEntity[0].author == resultBooks1[0].author)
        assertTrue(listBookEntity[0].title == resultBooks1[0].title)
        assertTrue(listBookEntity[0].cover == resultBooks1[0].cover)
        assertTrue(listBookEntity[1].author == resultBooks1[1].author)
        assertTrue(listBookEntity[1].title == resultBooks1[1].title)
        assertTrue(listBookEntity[1].cover == resultBooks1[1].cover)
    }

    @Test
    fun testGetBookDataDB() {
        val resultBooks2 = FakeDataProvider.getDataBooks(2)

        whenever(bookDataDBDataSource.getBooks(2)).thenReturn(Single.just(resultBooks2))
        whenever(bookDataDBDataSource.insert(any())).thenReturn(Completable.complete())

        whenever(bookDataNetworkDataSource.getBooks(2)).thenReturn(Single.just(arrayListOf()))

        val testObserver: TestObserver<List<Book>> = bookRepository.getBookByCategory(2).test()
        testObserver.assertNoErrors()
        testObserver.assertComplete()

        val listBookEntity = testObserver.events[0][0] as List<Book>

        assertTrue(listBookEntity.size == resultBooks2.size)
        assertTrue(listBookEntity[0].author == resultBooks2[0].author)
        assertTrue(listBookEntity[0].title == resultBooks2[0].title)
        assertTrue(listBookEntity[0].cover == resultBooks2[0].cover)
        assertTrue(listBookEntity[1].author == resultBooks2[1].author)
        assertTrue(listBookEntity[1].title == resultBooks2[1].title)
        assertTrue(listBookEntity[1].cover == resultBooks2[1].cover)
    }
}