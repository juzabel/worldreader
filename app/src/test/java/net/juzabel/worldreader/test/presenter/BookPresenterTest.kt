package net.juzabel.worldreader.test.presenter

import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import junit.framework.Assert.assertTrue
import net.juzabel.worldreader.domain.base.UseCase
import net.juzabel.worldreader.domain.entity.BookEntity
import net.juzabel.worldreader.presentation.Navigator
import net.juzabel.worldreader.presentation.contract.BookContract
import net.juzabel.worldreader.presentation.model.entity.Book
import net.juzabel.worldreader.presentation.model.mapper.BookMapper
import net.juzabel.worldreader.presentation.presenter.BookPresenter
import net.juzabel.worldreader.test.FakeDataProvider
import net.juzabel.worldreader.test.base.BaseTest
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class BookPresenterTest : BaseTest() {

    companion object {
        const val CATEGORY: Long = 1L
    }

    @Mock
    lateinit var navigator: Navigator
    @Mock
    lateinit var usecase: UseCase<Long, Single<List<BookEntity>>>
    @Mock
    lateinit var view: BookContract.View

    private lateinit var bookMapper: BookMapper
    private lateinit var bookPresenter: BookPresenter

    @Captor
    lateinit var captor: ArgumentCaptor<List<Book>>

    @Before
    fun setup() {

        MockitoAnnotations.initMocks(this)

        bookMapper = BookMapper()
        bookPresenter = BookPresenter(usecase, bookMapper, navigator)
        bookPresenter.view = view
    }

    @Test
    fun testGetBook() {

        val interactorValues = FakeDataProvider.getBookEntities()
        whenever(usecase.execute(CATEGORY)).thenReturn(Single.just(interactorValues))

        bookPresenter.getBooks(CATEGORY)
        verify(view).onBooksResult(captor.capture())

        val result: List<Book> = captor.value
        assertTrue(result.size == interactorValues.size)
        assertTrue(result[0].author == interactorValues[0].author)
        assertTrue(result[0].title == interactorValues[0].title)
        assertTrue(result[0].cover == interactorValues[0].cover)
        assertTrue(result[1].author == interactorValues[1].author)
        assertTrue(result[1].title == interactorValues[1].title)
        assertTrue(result[1].cover == interactorValues[1].cover)

    }
}