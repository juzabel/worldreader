package net.juzabel.worldreader.test.interactor

import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import junit.framework.Assert.assertTrue
import net.juzabel.worldreader.data.repository.CategoryRepository
import net.juzabel.worldreader.domain.entity.CategoryEntity
import net.juzabel.worldreader.domain.interactor.CategoryInteractor
import net.juzabel.worldreader.domain.mapper.CategoryDomainMapper
import net.juzabel.worldreader.test.FakeDataProvider
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class CategoryInteractorTest {

    @Mock
    lateinit var categoryRepository: CategoryRepository

    lateinit var categoryDomainMapper: CategoryDomainMapper
    lateinit var interactor: CategoryInteractor

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        categoryDomainMapper = CategoryDomainMapper()

        interactor = CategoryInteractor(categoryRepository, categoryDomainMapper)
    }

    @Test
    fun testGetCategoryDomain() {
        val resultCategories = FakeDataProvider.getDataCategories()

        whenever(categoryRepository.getCategories()).thenReturn(Single.just(resultCategories))

        val testObserver: TestObserver<List<CategoryEntity>> = interactor.execute(null).test()
        testObserver.assertNoErrors()
        testObserver.assertComplete()

        val listCategoryEntity = testObserver.events[0][0] as List<CategoryEntity>

        assertTrue(listCategoryEntity.size == resultCategories.size)
        assertTrue(listCategoryEntity[0].icon == resultCategories[0].icon)
        assertTrue(listCategoryEntity[0].title == resultCategories[0].title)
        assertTrue(listCategoryEntity[0].id == resultCategories[0].id)
        assertTrue(listCategoryEntity[1].icon == resultCategories[1].icon)
        assertTrue(listCategoryEntity[1].title == resultCategories[1].title)
        assertTrue(listCategoryEntity[1].id == resultCategories[1].id)
    }

}